package com.example.mextrafi_transitos.ui.registros;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RegistrosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RegistrosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Este es el fragment REGISTROS");
    }

    public LiveData<String> getText() {
        return mText;
    }
}