package com.example.mextrafi_transitos.ui.tools;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ToolsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ToolsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Este es el fragment HERRAMIENTAS");
    }

    public LiveData<String> getText() {
        return mText;
    }
}