package com.example.mextrafi_transitos.ui.multas;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MultasViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MultasViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Este es el fragment MULTAS");
    }

    public LiveData<String> getText() {
        return mText;
    }
}